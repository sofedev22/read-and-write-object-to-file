/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.filelab;

import java.io.Serializable;

/**
 *
 * @author Nippon
 */
public class Player implements Serializable{

    private int win;
    private char Symbol;
    private int loss;
    private int draw;

    public int getWin() {
        return win;
    }

    public void win() {
        this.win++;
    }

    public char getSymbol() {
        return Symbol;
    }

    public int getLoss() {
        return loss;
    }

    public void loss() {
        this.loss++;
    }

    public int getDraw() {
        return draw;
    }

    public void draw() {
        this.draw++;
    }

    public Player(char Symbol) {
        this.Symbol = Symbol;
    }
    

    public void setSymbol(char Symbol) {
        this.Symbol = Symbol;
    }

    @Override
    public String toString(){
        return "Player{" + "symbol=" + Symbol + ", win=" + win + ", loss=" + loss + ", draw=" + draw + '}';
    }
    
}
